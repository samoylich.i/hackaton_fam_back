from django.db import models


ROOM_TYPES = ["Ordinary", "Computer", "Sport"]
ROOM_TYPE = sorted((etype, etype) for etype in ROOM_TYPES)

LESSON_NUMBERS = ["First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth"]
LESSON_NUMBER = sorted((etype, etype) for etype in LESSON_NUMBERS)

DAY_LEVELS = ["Low", "Middle", "High"]
DAY_LEVEL = sorted((etype, etype) for etype in DAY_LEVELS)

class BlockModel(models.Model):
    name = models.CharField(max_length=20)


class SubjectModel(models.Model):
    name = models.TextField(max_length=30)
    block = models.ForeignKey(BlockModel, on_delete=models.CASCADE)
    is_subgroup_req = models.BooleanField(default=False,null=True, blank=True)
    difficulty_level = models.FloatField(default=1.0)

    def __str__(self):
        return self.name

class RoomModel(models.Model):
    room_type = models.CharField(choices=ROOM_TYPE, default='Ordinary', max_length=20)


class GroupModel(models.Model):
    class_number = models.CharField(max_length=3)

    def __str__(self):
        return self.class_number

class TeacherModel(models.Model):
    name = models.TextField(max_length=100)
    block = models.ForeignKey(BlockModel, on_delete=models.CASCADE)


class SubgroupModel(models.Model):
    subject = models.ForeignKey(SubjectModel, models.CASCADE)


class DayModel(models.Model):
    load_level = models.CharField(choices=DAY_LEVEL, max_length=10)


class LessonModel(models.Model):
    day = models.ForeignKey(DayModel, on_delete=models.CASCADE)
    lesson_number = models.IntegerField(choices=LESSON_NUMBER)
    group = models.ForeignKey(GroupModel, on_delete=models.CASCADE)
    subgroup = models.ForeignKey(SubgroupModel, on_delete=models.CASCADE)


class LoadModel(models.Model):
    group = models.ForeignKey(GroupModel, on_delete=models.CASCADE)
    subject = models.ForeignKey(SubjectModel, on_delete=models.CASCADE)
    hours = models.IntegerField()

