from django.contrib import admin
from schedule.models import (BlockModel, SubjectModel, GroupModel,
                             RoomModel, TeacherModel, SubgroupModel,
                             DayModel, LessonModel, LoadModel)
from schedule.first import load_teacher

# Register your models here.


class BlockAdmin(admin.ModelAdmin):
    def generate_teacher(self, request,objs):
        for obj in objs:
            load_teacher(obj)


    list_display = ("name", "pk")
    actions = [generate_teacher]


class SubjectAdmin(admin.ModelAdmin):
    list_display = ("name", "block", "is_subgroup_req", "difficulty_level")


class RoomAdmin(admin.ModelAdmin):
    list_display = ("room_type", "pk")


class GroupAdmin(admin.ModelAdmin):
    list_display = ("class_number", "pk")


class TeacherAdmin(admin.ModelAdmin):
    list_display = ("name", "block")


class SubgroupAdmin(admin.ModelAdmin):
    list_display = ("subject", "pk")


class DayAdmin(admin.ModelAdmin):
    list_display = ("load_level", "pk")


class LessonAdmin(admin.ModelAdmin):
    list_display = ("day", "lesson_number", "group", "subgroup")


class LoadAdmin(admin.ModelAdmin):
    list_display = ["group", "subject", "hours"]


admin.site.register(LessonModel, LessonAdmin)
admin.site.register(DayModel, DayAdmin)
admin.site.register(SubgroupModel, SubgroupAdmin)
admin.site.register(TeacherModel, TeacherAdmin)
admin.site.register(RoomModel, RoomAdmin)
admin.site.register(SubjectModel, SubjectAdmin)
admin.site.register(GroupModel, GroupAdmin)
admin.site.register(BlockModel, BlockAdmin)
admin.site.register(LoadModel, LoadAdmin)
