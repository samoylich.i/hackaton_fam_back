from schedule.models import TeacherModel, BlockModel, LoadModel
import random
import numpy as np


class GeneticAlgorithm:
    max_count = 27
    x = []

    def container_sum(self, container):
        s = 0
        for i in container:
            s += self.x[i]
        return s

    def get_container_distrib(self, order):

        new_x = self.order_x_indices(order)
        containers = [[]]
        for x in new_x:
            i = 0
            # print(x)
            while i < len(containers) and self.container_sum(containers[i]) + self.x[x] > self.max_count:
                # print(container[i])
                # print(x)
                i += 1
            if i == len(containers):
                containers.append([x])
            else:
                containers[i].append(x)
        return containers

    def get_container(self, order):
        #print(order)
        new_x = self.order_x(order)
        container = [0]
        for x in new_x:
            i = 0
            #print(x)
            while i < len(container) and container[i] + x > self.max_count:
                #print(container[i])
                #print(x)
                i += 1
            if i == len(container):
                container.append(x)
            else:
                container[i] += x
        return container

    def quality_function(self, order):
        result = 0
        container = self.get_container(order)
        for el in container:
            result += self.max_count - el
        return result

    def quality_function_distrib(self, order):
        result = 0
        containers = self.get_container_distrib(order)
        for el in containers:
            result += self.max_count - self.container_sum(el)
        return result

    def __init__(
            self,
            x,
            max_count = 27,
            population_size=100,
            worst_percentage=0.45,
            best_percentage=0.25,
            top_gen_use_percentage=0.75,
    ):
        self.x = x
        self.max_count = max_count
        self.population_size = population_size
        self.worst_percentage = worst_percentage
        self.best_percentage = best_percentage
        self.top_gen_use_percentage = top_gen_use_percentage
        self.vector_len = len(x)

    def get_min_pack_size(self):
        return len(self.get_container(self.get_optimal_parameters()))

    def get_min_pack_distrib(self):
        return self.get_container_distrib(self.get_optimal_parameters())

    def get_optimal_parameters(self, count_of_steps=100, quality_treshhold=0.01):
        population = [self.generate_population_element() for index in range(self.population_size)]
        population.sort(key=self.quality_function, reverse=True)
        quality = self.quality_function(population[-1])
        step = 0
        while step < count_of_steps and quality > quality_treshhold:
            next_population = []
            for index in range(self.population_size):
                if index < self.population_size * self.worst_percentage:
                    next_population.append(self.generate_population_element())
                elif index > self.population_size * (1.0 - self.best_percentage):
                    next_population.append(population[index])
                else:
                    next_population.append(self.crossing(population[index], population[-1]))
            step += 1
            population = next_population
            population.sort(key=self.quality_function, reverse=True)
            quality = self.quality_function(population[-1])
        return population[-1]

    def crossing(self, v_worst, v_best):
        v_result = []
        for i in range(len(v_worst)):
            if random.random() < self.top_gen_use_percentage:
                v_result.append(v_best[i])
            else:
                v_result.append(v_worst[i])
        return v_result

    def generate_population_element(self):
        result = []
        for index in range(self.vector_len):
            result.append(random.random())
        return result

    def order_x(self, order):
        order_copy = [x for x in order]
        result = []
        for i in range(len(order)):
            min_j = 0
            for j in range(len(order)):
                if order_copy[j] < order_copy[min_j]:
                    min_j = j
            result.append(self.x[min_j])
            order_copy[min_j] = 32767
        return result

    def order_x_indices(self, order):
        order_copy = [x for x in order]
        result = []
        for i in range(len(order)):
            min_j = 0
            for j in range(len(order)):
                if order_copy[j] < order_copy[min_j]:
                    min_j = j
            result.append(min_j)
            order_copy[min_j] = 32767
        return result

    def get_teacher_group_distribution(self):
        return {}



def load_teacher(block):
    load = LoadModel.objects.filter(subject__block=block)
    arr_load = [a.hours for a in load]
    ga = GeneticAlgorithm(arr_load, max_count=27)
    count_teacher = ga.get_min_pack_size()
    for i in range(count_teacher):
        TeacherModel.objects.create(name="teacher"+str(block)+str(i),
                                    block=block)